const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const IngredientSchema = new mongoose.Schema({
  ingTitle: String,
  amount: String
});

const CocktailSchema = new mongoose.Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  image: String,
  recipe: {
    type: String,
    required: true,
  },
  is_published: {
    type: Boolean,
    default: false
  },
  ingredients: [IngredientSchema]
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);
module.exports = Cocktail;