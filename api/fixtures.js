const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');
const User = require("./models/User");
const Cocktail = require("./models/Cocktail");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);
  const collections = await mongoose.connection.db.listCollections().toArray();
  for (const coll of collections) {
    await mongoose.connection.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@mail.ru',
    password: '123',
    avatar: 'user.jpg',
    displayName: 'Jack Doe',
    token: nanoid(),
    role: 'User'
  }, {
    email: 'admin@mail.ru',
    password: '456',
    avatar: 'admin.jpg',
    displayName: 'Admin',
    token: nanoid(),
    role: 'Admin'
  });

  await Cocktail.create({
    user: user,
    title: 'Old Fashioned',
    image: 'old_fashioned.jpg',
    recipe: 'Put sugar in glass. Cover it with dashes of bitters. Add whiskey and stir until sugar dissolves. Add' +
      ' ice, stir again, and serve. If the barman starts shaking the ingredients or muddling fruit, have your next round at another bar.',
    is_published: false,
    ingredients: [
      {ingTitle: 'Bourbon', amount: '2 oz'},
      {ingTitle: 'Angostura bitters', amount: '2 dashes'},
      {ingTitle: 'Sugar', amount: '1 tsp'},
      {ingTitle: 'Orange twist garnish', amount: '1 piece'}
    ]
  }, {
    user: admin,
    title: 'Margarita',
    image: 'margarita.jpg',
    recipe: 'Since this recipe includes fresh juice, it should be shaken. Serve over ice in a glass with a salted rim.',
    is_published: true,
    ingredients: [
      {ingTitle: 'Silver tequila', amount: '2 oz'},
      {ingTitle: 'Cointreau', amount: '1 oz'},
      {ingTitle: 'Lime juice', amount: '1 oz'}
    ]
  }, {
    user: admin,
    title: 'Cosmopolitan',
    image: 'cosmopolitan.jpg',
    recipe: 'Build all ingredients in a shaker tine with ice and shake. Strain into a martini glass and garnish with lime wheel or zest.',
    is_published: true,
    ingredients: [
      {ingTitle: 'Citrus vodka', amount: '1.5 oz'},
      {ingTitle: 'Cointreau', amount: '1 oz'},
      {ingTitle: 'Lime juice', amount: '1.5 oz'},
      {ingTitle: 'Cranberry juice', amount: '2.25 oz'}
    ]
  }, {
    user: user,
    title: 'Negroni',
    image: 'negroni.jpg',
    recipe: 'Stir ingredients with ice.',
    is_published: false,
    ingredients: [
      {ingTitle: 'Gin', amount: '1 oz'},
      {ingTitle: 'Campari', amount: '1 oz'},
      {ingTitle: 'Sweet vermouth', amount: '1 oz'},
    ]
  }, {
    user: admin,
    title: 'Moscow Mule',
    image: 'moscow_mule.jpg',
    recipe: 'Squeeze lime juice into a Moscow Mule mug. Add two or three ice cubes, pour in the vodka, and fill with cold ginger beer. Stir and serve.',
    is_published: true,
    ingredients: [
      {ingTitle: 'Vodka', amount: '2 oz'},
      {ingTitle: 'Ginger beer', amount: '4 oz'},
      {ingTitle: 'Lime juice', amount: '1.5 oz'},
    ]
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));