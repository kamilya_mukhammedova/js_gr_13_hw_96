const express = require('express');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const Cocktail = require("../models/Cocktail");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    if(req.query.user) {
      const userCocktails = await Cocktail.find({user: req.query.user});
      return res.send(userCocktails);
    }
    const cocktails = await Cocktail.find();
    return res.send(cocktails);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);
    if (!cocktail) {
      return res.status(404).send({message: 'Cocktail is not found'});
    }
    return res.send(cocktail);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title || !req.body.recipe || !req.body.ingredients) {
      return res.status(400).send({message: 'The title, recipe, ingredients are required'});
    }
    const ingredientsArray = JSON.parse(req.body.ingredients);
    const cocktailData = {
      user: req.user._id,
      title: req.body.title,
      recipe: req.body.recipe,
      image: null,
      ingredients: ingredientsArray
    };
    if (req.file) {
      cocktailData.image = req.file.filename;
    }
    if (req.user.role === 'Admin') {
      cocktailData.is_published = true;
    }
    const newCocktail = new Cocktail(cocktailData);
    await newCocktail.save();
    return res.send(newCocktail);
  } catch (e) {
    next(e);
  }
});

router.post('/:id/publish', auth, permit('Admin'), async (req, res, next) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);
    if(!cocktail) {
      return res.status(404).send({message: `Cocktail is not found, can't edit it`});
    }
    const editedCocktail = await Cocktail.findOneAndUpdate({_id: req.body.cocktailId}, {is_published: true},
      {new: true});
    return res.send(editedCocktail);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', auth, permit('Admin'), async (req, res, next) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);
    if(!cocktail) {
      return res.status(404).send({message: `Cocktail is not found, can't remove it`});
    }
    await Cocktail.deleteOne({_id: req.params.id});
    return res.send({message: `Cocktail with id ${req.params.id} has been removed!`});
  } catch (e) {
    next(e);
  }
});

module.exports = router;