import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Cocktail } from '../../models/cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute, Params } from '@angular/router';
import { fetchOneCocktailRequest } from '../../store/cocktails.actions';

@Component({
  selector: 'app-cocktail-details',
  templateUrl: './cocktail-details.component.html',
  styleUrls: ['./cocktail-details.component.sass']
})
export class CocktailDetailsComponent implements OnInit {
  cocktail: Observable<Cocktail | null>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {
    this.cocktail = store.select(state => state.cocktails.cocktail);
    this.loading = store.select(state => state.cocktails.fetchOneLoading);
    this.error = store.select(state => state.cocktails.fetchOneError);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const cocktailId = params['id'];
      this.store.dispatch(fetchOneCocktailRequest({cocktailId}));
    });
  }
}
