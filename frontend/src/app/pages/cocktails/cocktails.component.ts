import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Cocktail, CocktailDataForEdit } from '../../models/cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { editCocktailRequest, fetchCocktailsRequest, removeCocktailRequest } from '../../store/cocktails.actions';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.sass']
})
export class CocktailsComponent implements OnInit {
  cocktails: Observable<Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  editLoading: Observable<boolean>;
  editError: Observable<null | string>;
  removeLoading: Observable<boolean>;
  removeError: Observable<null | string>;

  constructor(private store: Store<AppState>) {
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.loading = store.select(state => state.cocktails.fetchLoading);
    this.error = store.select(state => state.cocktails.fetchError);
    this.editLoading = store.select(state => state.cocktails.editLoading);
    this.editError = store.select(state => state.cocktails.editError);
    this.removeLoading = store.select(state => state.cocktails.removeLoading);
    this.removeError = store.select(state => state.cocktails.removeError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCocktailsRequest());
  }

  onRemove(cocktailId: string) {
    this.store.dispatch(removeCocktailRequest({cocktailId}));
  }

  onEdit(cocktailId: string) {
    const data: CocktailDataForEdit = {cocktailId};
    this.store.dispatch(editCocktailRequest({cocktailData: data}));
  }
}
