import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CocktailData } from '../../models/cocktail.model';
import { CocktailsService } from '../../services/cocktails.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { createCocktailRequest } from '../../store/cocktails.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-new-cocktail',
  templateUrl: './new-cocktail.component.html',
  styleUrls: ['./new-cocktail.component.sass']
})
export class NewCocktailComponent implements OnInit {
  newCocktailForm!: FormGroup;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(
    private cocktailsService: CocktailsService,
    private store: Store<AppState>
    ) {
    this.loading = store.select(state => state.cocktails.createLoading);
    this.error = store.select(state => state.cocktails.createError);
  }

  ngOnInit(): void {
    this.newCocktailForm = new FormGroup({
      title: new FormControl('', [Validators.required, Validators.minLength(2)]),
      ingredients: new FormArray([]),
      recipe: new FormControl('', [Validators.required, Validators.minLength(5)]),
      image: new FormControl('', [Validators.minLength(2)]),
    });
    const ingredients = <FormArray>this.newCocktailForm.get('ingredients');
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.newCocktailForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  ingredientsFieldHasError(fieldName: string, index: number, errorType: string) {
    const ingredients = <FormArray>this.newCocktailForm.get('ingredients');
    const ingredientsArray = <FormGroup>ingredients.controls[index];
    const ingredientsField = ingredientsArray.get(fieldName);
    return Boolean(ingredientsField && ingredientsField.touched && ingredientsField.errors?.[errorType]);
  }

  formIsInvalid() {
    return this.newCocktailForm.invalid;
  }

  addIngredient() {
    const ingredients = <FormArray>this.newCocktailForm.get('ingredients');
    const ingredientsGroup = new FormGroup({
      ingTitle: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required)
    });
    ingredients.push(ingredientsGroup);
  }

  getIngredientsControls() {
    const ingredients = <FormArray>this.newCocktailForm.get('ingredients');
    return ingredients.controls;
  }

  onSubmit() {
    const cocktailData: CocktailData = {
      title: this.newCocktailForm.value.title,
      image: this.newCocktailForm.value.image,
      recipe: this.newCocktailForm.value.recipe,
      ingredients: JSON.stringify(this.newCocktailForm.value.ingredients)
    };
    this.store.dispatch(createCocktailRequest({cocktailData}));
  }
}
