import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable, Subscription } from 'rxjs';
import { Cocktail } from '../../models/cocktail.model';
import { fetchUserCocktailsRequest, removeCocktailRequest } from '../../store/cocktails.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-user-cocktails',
  templateUrl: './user-cocktails.component.html',
  styleUrls: ['./user-cocktails.component.sass']
})
export class UserCocktailsComponent implements OnInit, OnDestroy {
  cocktails: Observable<Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  user: Observable<null | User>;
  removeLoading: Observable<boolean>;
  removeError: Observable<null | string>;
  userSubscription!: Subscription;

  constructor(private store: Store<AppState>) {
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.loading = store.select(state => state.cocktails.fetchUserCocktailsLoading);
    this.error = store.select(state => state.cocktails.fetchUserCocktailsError);
    this.user = store.select(state => state.users.user);
    this.removeLoading = store.select(state => state.cocktails.removeLoading);
    this.removeError = store.select(state => state.cocktails.removeError);
  }

  ngOnInit(): void {
    this.userSubscription = this.user.subscribe(user => {
      if (user) {
        const userId = user?._id;
        this.store.dispatch(fetchUserCocktailsRequest({userId}));
      }
    });
  }

  onRemove(cocktailId: string) {
    this.store.dispatch(removeCocktailRequest({cocktailId}));
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
