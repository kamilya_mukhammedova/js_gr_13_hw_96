import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CocktailsComponent } from './pages/cocktails/cocktails.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { UserCocktailsComponent } from './pages/user-cocktails/user-cocktails.component';
import { NewCocktailComponent } from './pages/new-cocktail/new-cocktail.component';
import { CocktailDetailsComponent } from './pages/cocktail-details/cocktail-details.component';
import { RoleGuardService } from './services/role-guard.service';

const routes: Routes = [
  {path: '', component: CocktailsComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'user-cocktails',
    component: UserCocktailsComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['Admin', 'User']}
  },
  {
    path: 'new-cocktail',
    component: NewCocktailComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['Admin', 'User']}
  },
  {path: 'cocktail/:id', component: CocktailDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
