import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { AppStoreModule } from './store/app-store.module';
import { environment } from '../environments/environment';
import { AuthInterceptor } from './auth.interceptor';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { ImagePipe } from './pipes/image.pipe';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { CenteredCardComponent } from './ui/centered-card/centered-card.component';
import { CocktailsComponent } from './pages/cocktails/cocktails.component';
import { RegisterComponent } from './pages/register/register.component';
import { ValidateIdenticalDirective } from './validate-indentical.directive';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LoginComponent } from './pages/login/login.component';
import { UserCocktailsComponent } from './pages/user-cocktails/user-cocktails.component';
import { NewCocktailComponent } from './pages/new-cocktail/new-cocktail.component';
import { IsPublishedDirective } from './directives/is-published.directive';
import { CocktailDetailsComponent } from './pages/cocktail-details/cocktail-details.component';
import { MatSelectModule } from '@angular/material/select';
import { HasRolesDirective } from './directives/has-roles.directive';
import { ButtonsDirective } from './directives/buttons.directive';

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.fbAppID, {
        scope: 'email,public_profile'
      })
    }
  ]
};

@NgModule({
  declarations: [
    AppComponent,
    ImagePipe,
    FileInputComponent,
    CenteredCardComponent,
    CocktailsComponent,
    RegisterComponent,
    ValidateIdenticalDirective,
    LoginComponent,
    UserCocktailsComponent,
    NewCocktailComponent,
    IsPublishedDirective,
    CocktailDetailsComponent,
    HasRolesDirective,
    ButtonsDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppStoreModule,
    FlexLayoutModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    MatSnackBarModule,
    MatMenuModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSelectModule,
  ],
  providers: [
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
