export class Cocktail {
  constructor(
    public id: string,
    public user: string,
    public title: string,
    public image: string,
    public recipe: string,
    public is_published: boolean,
    public ingredients: {ingTitle: string, amount: string, _id: string}[]
  ) {}
}

export interface ApiCocktailData {
  _id: string,
  user: string,
  title: string,
  image: string,
  recipe: string,
  is_published: boolean,
  ingredients: {ingTitle: string, amount: string, _id: string}[]
}

export interface CocktailData {
  [key: string]: any,
  title: string,
  image: File | null,
  recipe: string,
  ingredients: string
}

export interface CocktailDataForEdit {
  cocktailId: string
}
