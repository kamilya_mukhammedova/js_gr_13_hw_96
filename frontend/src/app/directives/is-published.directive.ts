import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { Observable, Subscription } from 'rxjs';
import { User } from '../models/user.model';

@Directive({
  selector: '[appIsPublished]'
})
export class IsPublishedDirective implements OnInit, OnDestroy {
  @Input('appIsPublished') is_published!: boolean;
  userSubscription!: Subscription;
  user: Observable<null | User>;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private store: Store<AppState>
  ) {
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.userSubscription = this.user.subscribe(user => {
      this.viewContainer.clear();

      if (user && user.role === 'Admin') {
          this.viewContainer.createEmbeddedView(this.templateRef);
      } else if((user && user.role === 'User') || !user) {
        if(this.is_published) {
          this.viewContainer.createEmbeddedView(this.templateRef);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
