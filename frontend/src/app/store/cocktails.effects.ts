import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CocktailsService } from '../services/cocktails.service';
import {
  createCocktailFailure,
  createCocktailRequest,
  createCocktailSuccess,
  editCocktailFailure,
  editCocktailRequest,
  editCocktailSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchOneCocktailFailure,
  fetchOneCocktailRequest,
  fetchOneCocktailSuccess,
  fetchOUserCocktailsFailure,
  fetchUserCocktailsRequest,
  fetchUserCocktailsSuccess,
  removeCocktailFailure,
  removeCocktailRequest,
  removeCocktailSuccess
} from './cocktails.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { Router } from '@angular/router';
import { HelpersService } from '../services/helpers.service';
import { Store } from '@ngrx/store';
import { AppState } from './types';

@Injectable()
export class CocktailsEffects {
  constructor(
    private actions: Actions,
    private cocktailsService: CocktailsService,
    private router: Router,
    private helpers: HelpersService,
    private store: Store<AppState>
  ) {}

  fetchCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchCocktailsRequest),
    mergeMap(() => this.cocktailsService.getAllCocktails().pipe(
      map(cocktails => fetchCocktailsSuccess({cocktails})),
      catchError(() => of(fetchCocktailsFailure({
        error: 'Something went wrong with cocktails uploading!'
      })))
    ))
  ));

  fetchOneCocktail = createEffect(() => this.actions.pipe(
    ofType(fetchOneCocktailRequest),
    mergeMap(({cocktailId}) => this.cocktailsService.getOneCocktail(cocktailId).pipe(
      map(cocktail => fetchOneCocktailSuccess({cocktail})),
      catchError(() => of(fetchOneCocktailFailure({
        error: 'Something went wrong with one cocktail uploading!'
      })))
    ))
  ));

  createCocktail = createEffect(() => this.actions.pipe(
    ofType(createCocktailRequest),
    mergeMap(({cocktailData}) => this.cocktailsService.createNewCocktail(cocktailData).pipe(
      map(() => createCocktailSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Your cocktail is being reviewed by a moderator!');
        void this.router.navigate(['/']);
      }),
      catchError(() => of(createCocktailFailure({error: 'Wrong data of cocktail!'})))
    ))
  ));

  fetchUserCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchUserCocktailsRequest),
    mergeMap(({userId}) => this.cocktailsService.getUserCocktails(userId).pipe(
      map(cocktails => fetchUserCocktailsSuccess({cocktails})),
      catchError(() => of(fetchOUserCocktailsFailure({
        error: 'Something went wrong with user cocktails uploading!'
      })))
    ))
  ));

  editCocktail = createEffect(() => this.actions.pipe(
    ofType(editCocktailRequest),
    mergeMap(({cocktailData}) => this.cocktailsService.editCocktail(cocktailData).pipe(
      map(() => editCocktailSuccess()),
      tap(() => {
        this.store.dispatch(fetchCocktailsRequest());
        this.helpers.openSnackbar('Edit cocktail');
      }),
      catchError(() => of(editCocktailFailure({
        error: 'Something went wrong! Can\'t edit cocktail!'
      })))
    ))
  ));

  removeCocktail = createEffect(() => this.actions.pipe(
    ofType(removeCocktailRequest),
    mergeMap(({cocktailId}) => this.cocktailsService.removeCocktail(cocktailId).pipe(
      map(() => removeCocktailSuccess()),
      tap(() => {
        void this.router.navigate(['/']);
        this.store.dispatch(fetchCocktailsRequest());
        this.helpers.openSnackbar('Removed cocktail');
      }),
      catchError(() => of(removeCocktailFailure({
        error: 'Something went wrong! Can\'t remove cocktail!'
      })))
    ))
  ));
}
