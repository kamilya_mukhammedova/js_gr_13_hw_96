import { LoginError, RegisterError, User } from '../models/user.model';
import { Cocktail } from '../models/cocktail.model';

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
  fbLoading: boolean,
  fbError: null | string,
};

export type CocktailState = {
  cocktails: Cocktail[],
  cocktail: Cocktail | null,
  fetchLoading: boolean,
  fetchError: null | string,
  fetchOneLoading: boolean,
  fetchOneError: null | string,
  createLoading: boolean,
  createError: null | string,
  fetchUserCocktailsLoading: boolean,
  fetchUserCocktailsError: null | string,
  editLoading: boolean,
  editError: null | string,
  removeLoading: boolean,
  removeError: null | string,
};

export type AppState = {
  users: UsersState,
  cocktails: CocktailState
};
