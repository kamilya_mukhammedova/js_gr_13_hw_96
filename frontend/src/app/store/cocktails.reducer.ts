import { CocktailState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createCocktailFailure,
  createCocktailRequest,
  createCocktailSuccess,
  editCocktailFailure,
  editCocktailRequest,
  editCocktailSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchOneCocktailRequest,
  fetchOneCocktailSuccess,
  fetchOUserCocktailsFailure,
  fetchUserCocktailsRequest,
  fetchUserCocktailsSuccess,
  removeCocktailFailure,
  removeCocktailRequest,
  removeCocktailSuccess
} from './cocktails.actions';

const initialState: CocktailState = {
  cocktails: [],
  cocktail: null,
  fetchLoading: false,
  fetchError:  null,
  fetchOneLoading: false,
  fetchOneError: null,
  createLoading: false,
  createError: null,
  fetchUserCocktailsLoading: false,
  fetchUserCocktailsError: null,
  editLoading: false,
  editError: null,
  removeLoading: false,
  removeError: null,
};

export const cocktailsReducer = createReducer(
  initialState,
  on(fetchCocktailsRequest, state => ({...state, fetchLoading: true})),
  on(fetchCocktailsSuccess, (state, {cocktails}) => ({
    ...state,
    fetchLoading: false,
    cocktails
  })),
  on(fetchCocktailsFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(fetchOneCocktailRequest, state => ({
    ...state,
    fetchOneLoading: true,
  })),
  on(fetchOneCocktailSuccess, (state, {cocktail}) => ({
    ...state,
    fetchOneLoading: false,
    cocktail
  })),
  on(fetchCocktailsFailure, (state, {error}) => ({
    ...state,
    fetchOneLoading: false,
    fetchOneError: error
  })),
  on(createCocktailRequest, state => ({...state, createLoading: true})),
  on(createCocktailSuccess, state => ({...state, createLoading: false})),
  on(createCocktailFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),
  on(fetchUserCocktailsRequest, state => ({
    ...state,
    fetchUserCocktailsLoading: true,
  })),
  on(fetchUserCocktailsSuccess, (state, {cocktails}) => ({
    ...state,
    fetchUserCocktailsLoading: false,
    cocktails
  })),
  on(fetchOUserCocktailsFailure, (state, {error}) => ({
    ...state,
    fetchUserCocktailsLoading: false,
    fetchUserCocktailsError: error
  })),
  on(editCocktailRequest, state => ({
    ...state,
    editLoading: true,
  })),
  on(editCocktailSuccess, state => ({
    ...state,
    editLoading: false,
  })),
  on(editCocktailFailure, (state, {error}) => ({
    ...state,
    editLoading: false,
    editError: error
  })),
  on(removeCocktailRequest, state => ({
    ...state,
    removeLoading: true,
  })),
  on(removeCocktailSuccess, state => ({
    ...state,
    removeLoading: false,
  })),
  on(removeCocktailFailure, (state, {error}) => ({
    ...state,
    removeLoading: false,
    removeError: error
  })),
);
