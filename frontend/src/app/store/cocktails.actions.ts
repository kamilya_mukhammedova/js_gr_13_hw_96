import { createAction, props } from '@ngrx/store';
import { Cocktail, CocktailData, CocktailDataForEdit } from '../models/cocktail.model';

export const fetchCocktailsRequest = createAction('[Cocktails] Fetch Request');
export const fetchCocktailsSuccess = createAction(
  '[Cocktails] Fetch Success',
  props<{ cocktails: Cocktail[] }>()
);
export const fetchCocktailsFailure = createAction(
  '[Cocktails] Fetch Failure',
  props<{ error: string }>()
);
export const fetchOneCocktailRequest = createAction(
  '[Cocktail] Fetch Request',
  props<{ cocktailId: string }>()
);
export const fetchOneCocktailSuccess = createAction(
  '[Cocktail] Fetch Success',
  props<{ cocktail: Cocktail | null }>()
);
export const fetchOneCocktailFailure = createAction(
  '[Cocktail] Fetch Failure',
  props<{ error: string }>()
);
export const createCocktailRequest = createAction(
  '[Cocktail] Create Request',
  props<{ cocktailData: CocktailData }>()
);
export const createCocktailSuccess = createAction(
  '[Cocktail] Create Success'
);
export const createCocktailFailure = createAction(
  '[Cocktail] Create Failure',
  props<{ error: string }>()
);
export const fetchUserCocktailsRequest = createAction(
  '[User Cocktails] Fetch Request',
  props<{ userId: string }>()
);
export const fetchUserCocktailsSuccess = createAction(
  '[User Cocktails] Fetch Success',
  props<{ cocktails: Cocktail[] }>()
);
export const fetchOUserCocktailsFailure = createAction(
  '[User Cocktails] Fetch Failure',
  props<{ error: string }>()
);
export const editCocktailRequest = createAction(
  '[Cocktail] Edit Request',
  props<{ cocktailData: CocktailDataForEdit }>()
);
export const editCocktailSuccess = createAction(
  '[Cocktail] Edit Success'
);
export const editCocktailFailure = createAction(
  '[Cocktail] Edit Failure',
  props<{ error: string }>()
);
export const removeCocktailRequest = createAction(
  '[Cocktail] Delete Request',
  props<{cocktailId: string}>()
);
export const removeCocktailSuccess = createAction(
  '[Cocktail] Delete Success'
);
export const removeCocktailFailure = createAction(
  '[Cocktail] Delete Failure',
  props<{error: string}>()
);
