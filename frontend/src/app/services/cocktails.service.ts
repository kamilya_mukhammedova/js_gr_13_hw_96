import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiCocktailData, Cocktail, CocktailData, CocktailDataForEdit } from '../models/cocktail.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CocktailsService {
  constructor(private http: HttpClient) { }

  getAllCocktails() {
    return this.http.get<ApiCocktailData[]>(environment.apiUrl + '/cocktails').pipe(
      map(response => {
        return response.map(cocktailData => {
          return new Cocktail(
            cocktailData._id,
            cocktailData.user,
            cocktailData.title,
            cocktailData.image,
            cocktailData.recipe,
            cocktailData.is_published,
            cocktailData.ingredients
          );
        });
      })
    );
  }

  getOneCocktail(cocktailId: string) {
    return this.http.get<ApiCocktailData>(environment.apiUrl + `/cocktails/${cocktailId}`)
      .pipe(map(result => {
        if(!result) {
          return null;
        }
        return new Cocktail(
          result._id,
          result.user,
          result.title,
          result.image,
          result.recipe,
          result.is_published,
          result.ingredients
        );
      }));
  }

  getUserCocktails(userId: string) {
    return this.http.get<ApiCocktailData[]>(environment.apiUrl + `/cocktails?user=${userId}`).pipe(
      map(response => {
        return response.map(cocktailData => {
          return new Cocktail(
            cocktailData._id,
            cocktailData.user,
            cocktailData.title,
            cocktailData.image,
            cocktailData.recipe,
            cocktailData.is_published,
            cocktailData.ingredients
          );
        });
      })
    );
  }

  createNewCocktail(cocktailData: CocktailData) {
    const formData = new FormData();
    Object.keys(cocktailData).forEach(key => {
      if (cocktailData[key] !== null) {
        formData.append(key, cocktailData[key]);
      }
    });
    return this.http.post<ApiCocktailData>(environment.apiUrl + '/cocktails', formData);
  }

  editCocktail(cocktailData: CocktailDataForEdit) {
    return this.http.post<ApiCocktailData>(environment.apiUrl + `/cocktails/${cocktailData.cocktailId}/publish`, cocktailData);
  }

  removeCocktail(cocktailId: string) {
    return this.http.delete(environment.apiUrl + `/cocktails/${cocktailId}`);
  }
}
